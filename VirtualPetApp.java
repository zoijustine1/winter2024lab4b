import java.util.Scanner;

public class VirtualPetApp {

	public static void main(String[] args){
		// creating object, array and scanner
		RedPanda[] packOfRedPandas = new RedPanda[1];
		Scanner reader = new Scanner(System.in);
		
		// populating array
		for (int i = 0; i < packOfRedPandas.length; i++){
			
			System.out.println("Enter a cute name for the red panda:");
			String pandaName = reader.nextLine();
			
			System.out.println("Enter a food for the red panda to eat:");
			String pandaFood = reader.nextLine();

			System.out.println("Enter the number of hours the red panda will be asleep for:");
			int numOfHoursAsleep = Integer.parseInt(reader.nextLine());
			
			packOfRedPandas[i] = new RedPanda(pandaName, pandaFood, numOfHoursAsleep);
		}
		
		System.out.println("Enter a new cute name for the last panda:");
		String newName = reader.nextLine();
		System.out.println(packOfRedPandas[packOfRedPandas.length - 1].getName());
		System.out.println(packOfRedPandas[packOfRedPandas.length - 1].getFood());
		System.out.println(packOfRedPandas[packOfRedPandas.length - 1].getHoursAsleep());
		packOfRedPandas[packOfRedPandas.length - 1].setName(newName);
		System.out.println(packOfRedPandas[packOfRedPandas.length - 1].getName());
		System.out.println(packOfRedPandas[packOfRedPandas.length - 1].getFood());
		System.out.println(packOfRedPandas[packOfRedPandas.length - 1].getHoursAsleep());
		
	}
}
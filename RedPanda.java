public class RedPanda {
	private String name;
	private String food;
	private int hoursAsleep;
	
	public void wakeUpPanda(){
		if (hoursAsleep == 1){
			System.out.println(name + " has only been sleeping for " + hoursAsleep + " hour! Let " + name + " sleep!");
		}
		else if (hoursAsleep >= 0 && hoursAsleep <= 10){
			System.out.println(name + " has only been sleeping for " + hoursAsleep + " hours! Let " + name + " sleep!");
		}
		else {
			System.out.println(name + " has been sleeping for " + hoursAsleep + " hours! Wake up " + name + "!");
		}
	}
	
	public void eatFood(){
		System.out.println("You gave " + name + " some " + food + " to eat! Yum!");
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	public String getFood(){
		return this.food;
	}
	public int getHoursAsleep(){
		return this.hoursAsleep;
	}
	
	public RedPanda(String name, String food, int hoursAsleep){
		this.name = name;
		this.food = food;
		this.hoursAsleep = hoursAsleep;
	}
	
}